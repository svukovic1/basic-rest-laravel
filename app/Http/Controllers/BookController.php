<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Knjige;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $knjige = Knjige::all();

        return Response::json([
            'error' => false,
            'knjige' => $knjige->toArray()],
            200
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $knjiga = $request->json()->all();

        $knjigeSave = new Knjige();
        $knjigeSave['user_id'] = $knjiga['user_id'];
        $knjigeSave['naziv'] = $knjiga['naziv'];
        $knjigeSave['autor'] = $knjiga['autor'];
        $knjigeSave['jezik'] = $knjiga['jezik'];
        $knjigeSave['originalni_jezik'] = $knjiga['originalni_jezik'];
        $knjigeSave['godina_izdavanja'] = $knjiga['godina_izdavanja'];
       

        $knjigeSave->save();
        return Response::json([
            'error' => false,
            'knjige' => $knjiga],
            200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $knjige = Knjige::all();

        if ($knjige->isEmpty() == true) {
            return Response::json([
                'error' => true,
                'urls' => $knjige->toArray()],
                200
            );
        } else {
            return Response::json([
                'error' => false,
                'knjige' => $knjige->toArray()],
                200
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Check if user is authorized
        $knjiga = Knjige::where('user_id', Auth::user()->id)->find($id);

        if ($knjiga == null) {
            return json_encode([
                'error' => true,
                'message' => 'Vi niste vlasnik ove knjige!'],
                302
            );
        }

        $knjiga->naziv = $request->get('naziv');
        $knjiga->autor = $request->get('autor');
        $knjiga->jezik = $request->get('jezik');
        $knjiga->originalni_jezik = $request->get('originalni_jezik');
        $knjiga->godina_izdavanja = $request->get('godina_izdavanja');


        $knjiga->save();

        return Response::json([
            'error' => false,
            'message' => 'Informations are updated'],
            200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $knjiga = Knjige::where('user_id', Auth::user()->id)->find($id);

        if ($knjiga == null) {
            return json_encode([
                'error' => true,
                'message' => 'Vi niste vlasnik ove knjige!'],
                302
            );
        }

        $knjiga->delete();

        return json_encode([
            'error' => false,
            'message' => 'Knjiga je obrisana'],
            200
        );
    }

    /**
     * Get parameters for search
     *
     * @param   $autor $year
     * @return \Illuminate\Http\Response
     */
    public function search()
    {

        $author = Input::get('autor');
        $year = Input::get('godina_izdavanja');

        if ($author == null) {
            $knjige = Knjige::where('godina_izdavanja', $year)->get();
        } elseif ($year == null) {
            $knjige = Knjige::where('autor', $author)->get();
        }

        return Response::json([
            'error' => false,
            'knjige' => $knjige->toArray()],
            200
        );
    }
}
