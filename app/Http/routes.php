<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Testing authentication
Route::get('/authtest', ['middleware'=>'simpleauth', function()
 {
    return view('welcome');
 }]);

/*Route for Search By Author*/
Route::post('api/v1/knjige/search', 'BookController@searchByAuthor');

/*Route for Search By godina_izdavanja*/
Route::post('api/v1/knjige/search', 'BookController@search');

Route::group(['prefix' => 'api/v1', 'middleware'=>'simpleauth'], function()
{
    Route::resource('knjige', 'BookController');
});

Route::group(array('prefix' => 'api/v1'), function()
{
    Route::get('knjige', 'BookController@index');
});

Route::group(array('prefix' => 'api/v1'), function()
{
    Route::post('users', 'UserController@store');
});

