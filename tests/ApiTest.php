<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;

class ApiTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        Auth::loginUsingId(1);
    }

    public function testPostKnjige()
    {
        $knjige = [
            'naziv' => 'TestingBook',
            'autor' => 'Tester',
            'jezik' => 'Srpski',
            'originalni_jezik' => 'Srpski',
            'godina_izdavanja' => '1948',
            'user_id' => 1
        ];

        $this->post('/api/v1/knjige', $knjige)
            ->seeJson([
                'error' => false,
            ]);
        $this->seeInDatabase('knjige', ['naziv' => 'TestingBook']);

    }

    public function testUpdateKnjiga()
    {
        $knjige = [
            'naziv' => 'TestingBookUpdate',
            'autor' => 'TesterUpdate',
            'jezik' => 'Srpski1',
            'originalni_jezik' => 'Srpski1',
            'godina_izdavanja' => '1950',
            'user_id' => 1
        ];
        //Get TestingBook id
        $bookId = DB::table('knjige')->where('naziv', '=', 'TestingBook')->get();
        foreach ($bookId as $bookId) {
            $bookId->id;
        }
        $this->put('/api/v1/knjige/' . $bookId->id, $knjige)
            ->seeJson([
                'error' => false,
            ]);
    }

    public function testDeleteKnjiga()
    {
        //Get TestingBookUpdate id
        $bookId = DB::table('knjige')->where('naziv', '=', 'TestingBookUpdate')->get();
        foreach ($bookId as $bookId) {
            $bookId->id;
        }
        $this->delete('/api/v1/knjige/' . $bookId->id)
            ->seeJson([
                'error' => false,
            ]);
    }

    public function testPublicRouteForKnjige()
    {
        Auth::logout();
        $response = $this->call('GET', '/api/v1/knjige');
        $this->assertEquals(200, $response->status());
    }


}
