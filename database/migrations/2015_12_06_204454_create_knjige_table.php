<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKnjigeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('knjige', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('naziv');
            $table->string('autor');
            $table->string('jezik');
            $table->string('originalni_jezik');
            $table->integer('godina_izdavanja');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('knjige', function(Blueprint $table)
        {
            //
        });
    }
}
