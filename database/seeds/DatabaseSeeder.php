<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('users')->insert([
            'username' => 'admin',
            'password' => bcrypt('admin'),

        ]);
        DB::table('users')->insert([
            'username' => 'nomen',
            'password' => bcrypt('nescio'),
        ]);

        DB::table('knjige')->insert([
            'user_id' => 1,
            'naziv' => 'Na Drini cuprija',
            'autor' => 'Ivo Andric',
            'jezik' => 'Srpski',
            'originalni_jezik' => 'Srpski',
            'godina_izdavanja' => '1945',
        ]);

        DB::table('knjige')->insert([
            'user_id' => 1,
            'naziv' => 'The Hunger Games',
            'autor' => 'Suzanne Collins ',
            'jezik' => 'Engleski',
            'originalni_jezik' => 'Engleski',
            'godina_izdavanja' => '2008',
        ]);


        DB::table('knjige')->insert([
            'user_id' => 2,
            'naziv' => 'Deep Web - Mračna strana interneta',
            'autor' => 'Anonimus',
            'jezik' => 'Srpski',
            'originalni_jezik' => 'Srpski',
            'godina_izdavanja' => '2015',
        ]);

        DB::table('knjige')->insert([
            'user_id' => 2,
            'naziv' => 'Seobe',
            'autor' => 'Miloš Crnjanski',
            'jezik' => 'Srpski',
            'originalni_jezik' => 'Srpski',
            'godina_izdavanja' => '2015',
        ]);

        DB::table('knjige')->insert([
            'user_id' => 2,
            'naziv' => 'Miris kiše na Balkanu',
            'autor' => 'Gordana Kuić',
            'jezik' => 'Srpski',
            'originalni_jezik' => 'Srpski',
            'godina_izdavanja' => '1986',
        ]);


        Model::reguard();
    }
}
