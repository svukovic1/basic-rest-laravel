## Pokretanje aplikacije

# Nakon sto uradite git clone, potrebno je uraditi : 

1. composer install  - Instalira sve potrebne dependency-e

3. Kreirati DB -MySql

2. config\database - Podesiti parametre za DB

4. php artisan migrate  - kreiranje tabela u DB 

5. php artisan db:seed  - popni tabele sa podatcima

6. php artisan serve - localhost:8000 

Route : 

Knjige :

1. GET api/v1/knjige -  sve knjige - public

2. POST api/v1/knjige - dodavanje knjiga - potrebna autentifikacija

3. DELETE api/v1/knjige/{id} - brisanje knjige - potrebna autentifikacija i svaki korisnik moze brisati samo knjigu koju je on dodao

4. POST api/v1/knjige/search 
{autor} ili {godina_izdavanja} - poslati kao parametar

5. PUT api/v1/knjige/{id} - Update knjige - potrebna autentifikacija i svaki korisnik moze update samo knjigu koju je on dodao


Users :

6. POST api/v1/users - registracija korisnika

TESTOVI :

phpunit
